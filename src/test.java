
public class test {
	
	public static void main(String[] args) {
		
		int [] a = {1,2,3,5,6};
		
		System.out.println(test.ejemplo(a));
		
	}
	
	public static int ejemplo(int[] a) {
		int aux = 0;                                 
		for (int i = 0; i < a.length; i++) {         
		
			for (int j = 0; j < a.length; j++) {	 		
				aux = aux + a[i] + a[j];             
			}
			
		}
		return aux;								
	}
	
	public static int ejemplo2(int[] a) {
		
		int aux = 0;
		for (int i = 0; i < a.length; i++) {
			aux += a[i];
		}
		
		return aux * (a.length*2);
		
		
	}

}
