package ungs.programacioni.clases;

public class Clase3 {
    public static void main(String[] args) {
        int [] a = {1,2,3,4,5,6};
        imprimir(a);
        a = agregarUltimo(a,99);
        imprimir(a);

    }

    public static void imprimir(int[] a){
        System.out.print("{ ");
        for(int e: a){
            System.out.print(e + " ");
        }
        System.out.println("}");
    }

    public static int[] eliminarUltimo(int[]a){
        int [] b = new int[a.length-1];
        for (int i = 0; i < b.length; i++) {
            b[i] = a[i];
        }
        return b;

    }

    public static int[] agregarUltimo(int[]a, int x){
        int[] b = new int[a.length+1];
        for (int i = 0; i < a.length; i++) {
            b[i]=a[i];
        }
        b[b.length-1] = x;
        return b;
    }


}
