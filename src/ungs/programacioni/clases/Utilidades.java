package ungs.programacioni.clases;

public class Utilidades {

    public static void main(String[] args) {
        String vocales = "b";
        for (int i = 0; i < vocales.length(); i++) {
            System.out.println(esVocal(vocales.charAt(i)));
        }

    }

    public static String resto(String s){
        String b = "";
        for (int i = 1; i < s.length(); i++) {
            b+=s.charAt(i);
        }
        return b;
    }
    public static int[] resto(int[] a){
        int[] res = new int[a.length-1];
        for (int i = 0; i < res.length; i++) {
            res[i] = a[i+1];
        }
        return res;
    }

    public static boolean esVocal(char c){
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }
}
