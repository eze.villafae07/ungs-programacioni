package ungs.programacioni.clases;

public class Clase3ej1 {

    public static void main(String[] args) {
        int [] a = {1,2,3,4,5,6,7,8,9};
        revertir(a);
        for(int e:a){
            System.out.print(e + " ");
        }
    }

    public static int maximoElemento(int[] a){
        int maximo = a[0];
        for (int i = 0; i < a.length; i++) {
            if(a[i]>maximo){
                maximo = a[i];
            }
        }
        return maximo;
    }

    public static int[] reverso(int[] a){
        int [] b = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            b[(a.length-1)-i]= a[i];
        }
        return b;
    }

    public static void revertir(int[]a){
        int aux;
        for (int i = 0; i < a.length / 2; i++) {
            aux = a[i];
            a[i] = a[a.length - 1 - i];
            a[a.length - 1 - i] = aux;
        }

    }


}
