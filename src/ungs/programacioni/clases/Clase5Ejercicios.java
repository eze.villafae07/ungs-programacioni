package ungs.programacioni.clases;

public class Clase5Ejercicios {

    public static void main(String[] args) {
        int[] a = {1,2,3};
        System.out.println(pertenece(a,6));


    }

    public static void imprimirEspaciado(String s){
        if(s.length() == 1){
            System.out.println(s);
        }else {
            System.out.print(s.charAt(0)+" ");
            imprimirEspaciado(Utilidades.resto(s));
        }
    }

    public static String inverso(String s){
        if(s.length()==1){
            return s;
        }else {
            String uLetra = String.valueOf(s.charAt(0));
            return inverso(Utilidades.resto(s)) + uLetra;
        }
    }

    public static String combinar(String s1, String s2){
        if(s1.length() == 1 && s2.length()>1) {
            return s1.charAt(0) + s2;
        }else if(s2.length() == 1 && s1.length()>1){
            return s2.charAt(0) + s1;
        }else {
            char minimo = s1.charAt(0)<s2.charAt(0) ? s1.charAt(0) : s2.charAt(0);
            return minimo + combinar(Utilidades.resto(s1),Utilidades.resto(s2));
        }
    }

    public static boolean esAbecedaria(String s){
        if(s.length()<=1){
            return true;
        }else {
            return s.charAt(0)<=s.charAt(1) && esAbecedaria(Utilidades.resto(s));
        }
    }

    public static void imprimir(int[] a){
        if(a.length == 1){
            System.out.println(a[0]);
        }else {
            System.out.print(a[0]);
            imprimir(Utilidades.resto(a));
        }
    }

    public static int suma(int[] a){
        if(a.length == 1){
            return a[0];
        }else{
            return a[0] + suma(Utilidades.resto(a));
        }
    }

    public static boolean pertenece(int[] a, int n){
        if(a.length==0){
            return false;
        }else{
            return a[0]==n || pertenece(Utilidades.resto(a),n);
        }

    }





}
