package ungs.programacioni.clases;

public class Clase4 {

    /*
    Ejercicio 1: void imprimirHasta(int n)
Imprime por pantalla los números entre 1 y n (en orden ascendente).

Ejercicio 2:void imprimirDesde(int n)
Imprime por pantalla los números entre 1 y n (en orden descendente).

Ejercicio 3: int sumaDesdeUnoHasta(int n)
Devuelve la suma de todos los numeros entre 1 y n

Ejercicio 4: boolean esPar(int n)
Indica si un número es par o no. Resolverlo de forma recursiva.

Ejercicio 5: boolean esImpar(int n)
Indica si un número es impar o no. Idem ejercicio 4.

Ejercicio 6: int sumaDeCuadradosHasta(int n)
Devuelve la suma de todos los numeros elevados al cuadrado entre 1 y n.
     */

    public static void main(String[] args) {
        System.out.println(sumaCuadradosHasta(3));
    }

    public static void imprimirDesde(int n){

        if(n>=1){
            System.out.println(n);
            imprimirDesde(n-1);
        }
    }

    public static void imprimirHasta(int n){
        if(n==1){
            System.out.println(n);
        }else {
            imprimirHasta(n-1);
            System.out.println(n);
        }


    }

    public static int sumaDesdeUnoHasta(int n){
        if(n==1){
            return n;
        }else{
            return n + sumaDesdeUnoHasta(n-1);
        }
    }

    public static boolean esPar(int n){
        if(n==0){
            return true;
        }else if(n==1){
            return false;
        }else {
            return esPar(n-2);
        }

    }

    public static boolean esImpar(int n){
        if(n==1){
            return true;
        }else if(n==0){
            return false;
        }else{
            return esImpar(n-2);
        }
    }

    public static int sumaCuadradosHasta(int n){
        if(n==1){
            return n*n;
        }else{
            return (n*n) + sumaCuadradosHasta(n-1);
        }
    }




}
