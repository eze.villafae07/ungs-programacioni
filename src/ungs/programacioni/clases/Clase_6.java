package ungs.programacioni.clases;

public class Clase_6 {

    public static void main(String[] args) {
        System.out.println(alternaVocales("ropero","mulata"));
    }

    public static String tomarCaracteresDesde(String s, int desde, int cant){

        if(s.length()<=desde || cant == 0){
            return "";
        }else{
            return s.charAt(desde) + tomarCaracteresDesde(s,desde+1,cant-1);
        }
    }


    // s = ropero t = mula
    // return s.charat(0) + alternarvocales(resto(s), resto(t)) ---> s.charat(0) no es vocal
    //                                                  -----> t.charat(0) no es vocal
    // return t.charat(0) + alternarVocales(resto(s),resto(t)) ---> s.charat(0) es vocal
    //                                                         ----> t.charat(0) es vocal
    //

    public static String alternaVocales(String s, String t){
        if(t.length()==0){
            return s;
        }else if(s.length()==0){
            return "";
        }else {
            if(esVocal(s.charAt(0)) && esVocal(t.charAt(0))){
                return t.charAt(0) + alternaVocales(Utilidades.resto(s),Utilidades.resto(t));
            }else if(!(esVocal(s.charAt(0))) && esVocal(t.charAt(0))){
                return s.charAt(0) + alternaVocales(Utilidades.resto(s),Utilidades.resto(t));
            }else if(esVocal(s.charAt(0)) && !(esVocal(t.charAt(0)))){
                return alternaVocales(s,Utilidades.resto(t));
            }else {
                return s.charAt(0) + alternaVocales(Utilidades.resto(s),Utilidades.resto(t));
            }
        }
    }

    public static boolean esVocal(char c){
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }




}
