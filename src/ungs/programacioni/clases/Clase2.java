package ungs.programacioni.clases;

public class Clase2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String texto = "Programacion";
		
		//recorrerTextoAlReves(texto);
		
		System.out.println(cantLetras(texto, 'o'));

	}
	
	
	public static void recorrerTexto(String texto) {
		for (int i = 0; i<texto.length();i++) {
			System.out.println(texto.charAt(i));
		}
	}
	
	public static void recorrerTextoAlReves(String texto) {
		for (int i = texto.length()-1; i>=0;i--) {
			System.out.print(texto.charAt(i));
		}
	}
	
	public static int cantLetras(String texto, char letra) {
		texto = texto.toLowerCase();
		int cont = 0;
		for(int i = 0; i<texto.length();i++) {
			if(texto.charAt(i)=='a') {
				cont++;
			}
		}
		return cont;
	}

}
