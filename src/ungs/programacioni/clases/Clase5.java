package ungs.programacioni.clases;

import ungs.programacioni.practica0.Ejercicio_34;

public class Clase5 {

    public static void main(String[] args) {
        System.out.println(longitud("hola"));
        System.out.println(todosIguales("oooooo"));
        int[] a = {1,2,3,4,5,6,7};
        System.out.println(maximo(a));
    }

    public static int longitud(String s){
        if(s.equals("")){
            return 0;
        }else{
            return 1 + longitud(Ejercicio_34.resto(s));
        }
    }

    public static boolean todosIguales(String s){
        if(s.length()<2){
            return true;
        }else {
            return s.charAt(0)==s.charAt(1) && todosIguales(Ejercicio_34.resto(s));
        }
    }

    public static int maximo(int[] a){
        if(a.length == 1){
            return a[0];
        }else{
            int maximoDelResto = maximo(Utilidades.resto(a));
            return a[0] > maximoDelResto ? a[0] : maximoDelResto;
        }
    }
}
