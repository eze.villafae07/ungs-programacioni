package ungs.programacioni.clases;

public class Clase2ej1 {
	
	public static void main(String[] args) {
		
		System.out.println(todasE("EeeeeE"));
		
	}
	
	public static boolean todasE(String texto) {
		texto = texto.toLowerCase();
		boolean hayE = true;
		for (int i = 0; i<texto.length(); i++) {
			if(texto.charAt(i)!='e' ) {
				hayE=false;
			}
		}
		
		return hayE;
		
	}

}
