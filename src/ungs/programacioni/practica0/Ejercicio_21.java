package ungs.programacioni.practica0;

public class Ejercicio_21 {

    public static void main(String[] args) {
        System.out.println(esSinRepetidos("aaa"));
    }

    public static boolean esSinRepetidos(String s){
        for(int i = 0; i<s.length();i++){
            if(Ejercicio_17.cantApariciones(s,s.charAt(i))>1){
                return false;
            }
        }
        return true;
    }



}
