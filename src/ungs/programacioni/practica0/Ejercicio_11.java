package ungs.programacioni.practica0;

public class Ejercicio_11 {
	
	public static void main(String[] args) {
		System.out.println(sumatoriaPares(10));
	}
	
	static int sumatoriaPares(int n){
		int suma = 0;
		for(int i = 2; i <= n; i++) {
			if (i%2==0) {
				suma = suma + i;
			}
		}
		
		return suma;
	}

}
