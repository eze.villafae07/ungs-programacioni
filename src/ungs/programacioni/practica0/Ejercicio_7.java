package ungs.programacioni.practica0;

import java.util.Scanner;

public class Ejercicio_7 {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		imprimirPromedio(scan.nextInt(), scan.nextInt());
	}
	
	static void imprimirPromedio(int a, int b) {
		System.out.println("Promedio : " + (a+b)/2);
	}
}
