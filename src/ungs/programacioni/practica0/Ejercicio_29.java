package ungs.programacioni.practica0;

public class Ejercicio_29 {
    public static void main(String[] args) {

        double [] a = {7,5,9};
        System.out.println(promedio(a));

    }

    public static double promedio(double[] a){
        double sumaElementos = 0;
        for (double e: a){
            sumaElementos+=e;
        }
        return sumaElementos / (a.length);
    }
}
