package ungs.programacioni.practica0;



public class Ejercicio_20 {
    public static void main(String[] args) {
        System.out.println(esCapicua("oso"));
    }

    public static boolean esCapicua(String s){
        s = s.toLowerCase();
        int cont = s.length()-1;
        for (int i = 0; i<s.length();i++){
            if(s.charAt(i)==s.charAt(cont)){
                cont--;
            }else{
                return false;
            }
        }
        return true;
    }
}
