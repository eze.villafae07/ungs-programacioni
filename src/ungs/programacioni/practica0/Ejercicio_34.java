package ungs.programacioni.practica0;

public class Ejercicio_34 {

    public static void main(String[] args) {
        imprimirConAsteriscos("hola");
        System.out.println("hola");
    }

    public static String resto(String s){
        String b = "";
        for (int i = 1; i < s.length(); i++) {
            b+=s.charAt(i);
        }
        return b;
    }

    public static void imprimirConAsteriscos(String s){
        if(s.length()==1){
            System.out.println(s);
        }else{
            System.out.print(s.charAt(0)+"*");
            imprimirConAsteriscos(resto(s));
        }
    }


}
