package ungs.programacioni.practica0;

public class Ejercicio_17 {

    public static void main(String[] args) {

        System.out.println(cantApariciones("hola",'a'));

    }

    public static int cantApariciones(String s, char c){
        int cont = 0;
        for (int i = 0; i<s.length();i++){
            if (s.charAt(i)==c){
                cont++;
            }
        }
        return cont;
    }


}
