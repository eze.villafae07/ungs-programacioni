package ungs.programacioni.practica0;

public class Ejercicio_13 {
	public static void main(String[] args) {
		System.out.println(factorial(0));
	}
	static double factorial(int n) {
		int factorial = 1;
		
		while(n!=0) {
			factorial = factorial * n;
			n--;
		}
		
		return factorial;
	}
}
