package ungs.programacioni.practica0;

public class Ejercicio_26 {
    public static void main(String[] args) {
        int [] a = new int[4];
        a[0]=10;
        a[1]=2;
        a[2]=3;
        a[3]=5;
        System.out.println(maximoIndice(a));

    }
    public static int maximoIndice(int[] a){
        int max = a[0];
        int posMax=0;
        for(int i = 0; i<a.length;i++){
            if(a[i]>max){
                posMax = i;
            }
        }
        return posMax;
    }
}
