package ungs.programacioni.practica0;

public class Ejercicio_18 extends Ejercicio_17{

    public static void main(String[] args) {
        System.out.println(cantVocales("hola"));

    }

    public static int cantVocales(String s){
        int cont = 0;
        String vocales = "aeiou";

        for(int i = 0;i<vocales.length();i++){
            if(cantApariciones(s,vocales.charAt(i))>0){
                cont++;
            }
        }
        return cont;


    }

}
