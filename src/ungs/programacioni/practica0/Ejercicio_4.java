package ungs.programacioni.practica0;

public class Ejercicio_4 {
	public static void main(String[] args) {
		System.out.println(1/2); //dividir dos enteros da como resultado un entero
		System.out.println(1.0/2.0); // dividir dos numeros decimales da como resultado un numero decimal
		System.out.println(1.0/2);
		System.out.println(1/2.0);
		//System.out.println("1"/"2"); // no se puede hacer operaciones matematicas con strings
		System.out.println(1+2);
		System.out.println("1"+ "2"); // el + concatena 
		System.out.println(16/2*4);
		System.out.println(16/(2*4));
	}
}
