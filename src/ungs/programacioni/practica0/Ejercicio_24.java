package ungs.programacioni.practica0;

public class Ejercicio_24 {
    public static void main(String[] args) {

        System.out.println(esDobleCapicua("neuquenoro"));

    }


    public static boolean esDobleCapicua(String s){
        for (int i = 0;i<=s.length();i++){
            if(Ejercicio_20.esCapicua(s.substring(0,i)) && Ejercicio_20.esCapicua(s.substring(i,s.length()))){
                return true;
            }
        }
        return false;
    }

}
