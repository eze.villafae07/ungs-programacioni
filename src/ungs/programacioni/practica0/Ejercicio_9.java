package ungs.programacioni.practica0;

import java.util.Scanner;

public class Ejercicio_9 {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Escriba dia, mes, año como numeros");
		imprimirFecha(scan.nextInt(), scan.nextInt(), scan.nextInt());
	}
	
	static void imprimirFecha(int dia, int mes, int anio) {
		String textoMes = "";
		
		switch(mes) {
		case 1:
			textoMes = "Enero";
			break;
		case 2:
			textoMes = "Febrero";
			break;
		case 3:
			textoMes = "Marzo";
			break;
		case 4:
			textoMes = "Abril";
			break;
		case 5:
			textoMes = "Mayo";
			break;
		case 6:
			textoMes = "Junio";
			break;
		case 7:
			textoMes = "Julio";
			break;
		case 8:
			textoMes = "Agosto";
			break;
		case 9:
			textoMes = "Septiembre";
			break;
		case 10:
			textoMes = "Octubre";
			break;
		case 11:
			textoMes = "Noviembre";
			break;
		case 12:
			textoMes = "Diciembre";
			break;
		default:
			System.err.print("Mes no valido");
		}
		
		System.out.println(dia + " de " + textoMes + " de " + anio);
	}

}
