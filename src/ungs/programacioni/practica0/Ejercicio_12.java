package ungs.programacioni.practica0;

public class Ejercicio_12 {
	
	public static void main(String[] args) {
		System.out.println(potencia(2, 3));
	}
	
	static double potencia(double x, int a) {
		double resultado = 1;
		for(int i = 1; i <= a; i++) {
			resultado = resultado * x;
			
		}
		
		return resultado;
	}

}
