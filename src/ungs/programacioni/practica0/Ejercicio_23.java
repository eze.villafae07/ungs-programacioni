package ungs.programacioni.practica0;

public class Ejercicio_23 {
    public static void main(String[] args) {

        System.out.println(puedenColorcarse("hi","hola","anana"));

    }


    public static boolean puedenColorcarse(String a, String b, String c){
        boolean puedeColocarseA = false;
        boolean puedeColocarseB = false;
        int i = -1;
        while(i<b.length()){
            i++;
            if(!puedeColocarseA){
                if(contieneLetra(a,b.charAt(i))){
                    puedeColocarseA = true;
                    i+=2;
                    if(i>=b.length()){
                        return  false;
                    }
                }
            }

            if(puedeColocarseA && !puedeColocarseB){
                if(contieneLetra(c,b.charAt(i))){
                    puedeColocarseB = true;
                }
            }

        }

        if (puedeColocarseA && puedeColocarseB){
            return true;
        }else {
            return false;
        }
    }

    public static boolean contieneLetra(String s, char letra){
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)==letra){
                return true;
            }
        }

        return false;
    }

}
