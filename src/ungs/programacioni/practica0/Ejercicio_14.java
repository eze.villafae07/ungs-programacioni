package ungs.programacioni.practica0;

public class Ejercicio_14 {
	
	public static void main(String[] args) {
		System.out.println(cantCifras(5000000));
	}
	
	static int cantCifras(int n) {
		int cifras = 0;
		while(n!=0){
			n = n/10;
			cifras++;
		}
		return cifras;
	}

}
