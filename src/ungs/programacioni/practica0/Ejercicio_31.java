package ungs.programacioni.practica0;

public class Ejercicio_31 {

    public static void main(String[] args) {

        for (int i = 0; i<50;i++){
            System.out.println(fibonacci(i));
        }
    }

    public static long fibonacci(int n){
        return n<2 ? n : fibonacci(n-1) + fibonacci(n-2);
    }

    public static int fibrec(int n){

        if(n>=2){
            return fibrec(n-1) + fibrec(n-2);
        }else if(n==1){
            return 1;
        }else if(n==0){
            return 0;
        }else {
            System.out.println("err");
            return -1;
        }

    }

    public static int fibiter(int n){
        int a = 0, b = 1, suma = 1;
        for (int i = 0; i<n;i++){
            suma = a+b;
            a = b;
            b = suma;
        }
        return suma;
    }

}
