package ungs.programacioni.practica0;

public class Ejercicio_19 {
    public static void main(String[] args) {
        System.out.println(esAbecedaria("himno"));
    }

    public static boolean esAbecedaria(String s) {
        boolean esAbc = true;
        char letra_actual;
        char letra_siguiente;
        for(int i = 0;i<s.length()-1;i++){
            letra_actual = quitarTilde(s.charAt(i));
            letra_siguiente = quitarTilde(s.charAt(i+1));
            if(letra_actual>letra_siguiente){
                esAbc = false;
                return esAbc;
            }
        }
        return esAbc;
    }

    public static char quitarTilde(char letra){
        switch (letra){
            case 'á':
                letra = 'a';
                break;
            case 'é':
                letra = 'e';
                break;
            case 'í':
                letra = 'i';
                break;
            case 'ó':
                letra = 'o';
                break;
            case 'ú':
                letra = 'u';
                break;
        }
        return letra;
    }

}
