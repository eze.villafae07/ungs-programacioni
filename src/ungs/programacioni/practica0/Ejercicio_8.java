package ungs.programacioni.practica0;

import java.util.Scanner;

public class Ejercicio_8 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese las dos notas");
		ponerNota(scan.nextFloat(), scan.nextFloat());
	}
	
	
	static void ponerNota(double x, double y){
		double promedio = (x+y)/2;
		Ejercicio_7.imprimirPromedio((int)x, (int)y);
		if (promedio >= 7) {
			System.out.println("Promocionado");
		}else if(promedio >=4) {
			System.out.println("Aprobado");
		}else {
			System.out.println("Debe recuperar");
		}
	}
}
