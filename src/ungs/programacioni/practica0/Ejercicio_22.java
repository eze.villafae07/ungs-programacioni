package ungs.programacioni.practica0;

public class Ejercicio_22 {

    public static void main(String[] args) {

        System.out.println(sinRepetidos("hooooollllaaaaa"));

    }

    public static String sinRepetidos(String s){
        String nueva_cadena = "";

        for(int i = 0;i<s.length();i++){
            if(Ejercicio_17.cantApariciones(nueva_cadena,s.charAt(i))==0){
                nueva_cadena = nueva_cadena + s.charAt(i);
            }
        }

        return nueva_cadena;

    }
}
