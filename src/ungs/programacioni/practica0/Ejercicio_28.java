package ungs.programacioni.practica0;

public class Ejercicio_28 {
    public static void main(String[] args) {

        int[] a = {1,2,3,4,5,3,7,8,9,10};
        System.out.println(estaOrdenado(a));

    }
    public static boolean estaOrdenado(int[] a){
        boolean ordenado = true;
        for(int i = 0; i<a.length-1;i++) {
            if (a[i] > a[i + 1]) {
                return false;
            }
        }
        return true;
    }
}
