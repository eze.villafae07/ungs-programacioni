package ungs.programacioni.practica0;

public class Ejercicio_10 {
	public static void main(String[] args) {
		System.out.println(sumatoria(10));
	}
	
	
	static int sumatoria(int n) {
		int suma = 0;
		for (int i = 1; i <= n; i++) {
			suma = suma + i;
		}
		return suma;
	}

}
