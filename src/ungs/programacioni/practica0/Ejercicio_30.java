package ungs.programacioni.practica0;

public class Ejercicio_30 {
    public static void main(String[] args) {
        System.out.println(potenciaRec(2,5));
    }

    public static int sumatoriaRec(int a){
        if(a>1){
            int sumatoria = a + sumatoriaRec(a-1);
            return sumatoria;
        }
        return 1;
    }

    public static int sumatoriaParesRec(int a){
        if (a%2!=0){
            a--;
        }
        if (a>2){
            int sumatoria = a + sumatoriaParesRec(a-2);
            return sumatoria;
        }

        return 2;
    }

    public static void imprimir(int x){
        if(x<=5){
            System.out.println(x);
            imprimir(x + 1);
        }
    }

    public static int factorial(int n){
        /*
        if (a>0){
            int valor_calculado = a * factorial(a-1);
            return valor_calculado;
        }
        return 1;*/
        if(n==0){
            return 1;
        }else
            return n * factorial(n-1);
    }

    public static double potenciaRec(double x, int n){
        if(n==0){
            return 1;
        }else if(n==1){
            return x;
        }else {
            return x * potenciaRec(x,n-1);
        }

    }

}
