package ungs.programacioni.practica0;

public class Ejercicio_32 {
    public static void main(String[] args) {
        collatz(400);
    }

    public static void collatz(int n){
        if(n==1){
            System.out.println(n);
        }else if(n%2==0){ // par
            System.out.println(n);
            collatz(n/2);
        }else{ // impar
            System.out.println(n);
            collatz((3*n)+1);
        }
    }

}
