package ungs.programacioni.practica0;

public class Ejercicio_33 {
    public static void main(String[] args) {
        System.out.println(mcd(87,27));
    }

    public static int mcd(int a, int b){

        if(a%b==0){
            return b;
        }else {
            int r = a%b;
            return mcd(b,r);
        }

    }
}
