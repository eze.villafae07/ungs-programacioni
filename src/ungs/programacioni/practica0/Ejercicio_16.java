package ungs.programacioni.practica0;

import java.util.Scanner;
import ungs.programacioni.clases.Clase2;

public class Ejercicio_16 extends Clase2{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Escriba un texto");
		//Clase2.recorrerTextoAlReves(sc.nextLine());

		System.out.println(reversa(sc.nextLine()));

	}

	public static String reversa(String cadena){
		/*
		  toma una String y devuelve el string dado vuelta
		  hola -> aloh
		 */
		String cadena_nueva = "";

		for (int i = cadena.length()-1;i>=0;i--){
			cadena_nueva = cadena_nueva + cadena.charAt(i);
		}
		return cadena_nueva;
	}
}
