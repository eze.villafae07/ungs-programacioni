package ungs.programacioni.practica0;

import java.util.Scanner;

public class Ejercicio_3 {

	public static void main(String[] args) {
		System.out.println("Escriba 2 numeros y los sumaré");
		Scanner scan = new Scanner(System.in);
		int numero1 = scan.nextInt();
		int numero2 = scan.nextInt();
		
		int result = numero1 + numero2;
		System.out.println("La suma de los dos números es: " + result);
	}

}
