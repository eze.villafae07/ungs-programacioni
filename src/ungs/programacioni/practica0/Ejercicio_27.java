package ungs.programacioni.practica0;

public class Ejercicio_27 {
    public static void main(String[] args) {
        int[] a = {1,2,3};
        System.out.println(suma(a));
    }

    public static int suma(int[] a){
        int sumaElementos = 0;
        for (int e: a){
            sumaElementos+=e;
        }

        return sumaElementos;
    }
}
