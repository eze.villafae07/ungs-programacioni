package ungs.programacioni.practica0;

public class Ejercicio_25 {

    public static void main(String[] args) {
        int[] a = {1,2,3,4,5};
        System.out.println(maximo(a));

    }

    public static int maximo(int[] a){
        int max = a[0];

        for (int e : a) {
            if (e > max) {
                max = e;
            }
        }
        return max;
    }
}
