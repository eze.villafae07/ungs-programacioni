package ungs.programacioni.practica0;

public class Ejercicio_35 {

    public static void main(String[] args) {

        System.out.println(sinCaracteresRepetidos("aaeeoo"));
    }

    public static String sinCaracteresRepetidos(String s){
        if(s.length()>1){
            if(s.charAt(0)==s.charAt(1)){
                return sinCaracteresRepetidos(Ejercicio_34.resto(s));
            }else {
                return String.valueOf(s.charAt(0))+sinCaracteresRepetidos(s.substring(1));
            }
        }else {
            return s;
        }
    }

}
